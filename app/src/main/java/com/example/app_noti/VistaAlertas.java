package com.example.app_noti;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.app_noti.Api.Api;
import com.example.app_noti.Api.Servicios.ServicioPeticion;
import com.example.app_noti.Models.M_TodasAlertas;
import com.example.app_noti.ViewModels.Todas_Alertas;

import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VistaAlertas extends AppCompatActivity {

    //Variables para la ListView
    private ListView ListaAlertas;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vista_alertas);

        //Enlazar variable con la vista del ListView
        ListaAlertas = findViewById(R.id.ListV_Alertas);

        // Verificar token
        /*SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN", "");
        if (token != "") {
            Toast.makeText(VistaAlertas.this, "Hay un token valido", Toast.LENGTH_SHORT).show();
        }*/

        ServicioPeticion service = Api.getApi(VistaAlertas.this).create(ServicioPeticion.class);
        Call<Todas_Alertas>  todas_alertasCall = service.getAlertas();
        todas_alertasCall.enqueue(new Callback<Todas_Alertas>() {
            @Override
            public void onResponse(Call<Todas_Alertas> call, Response<Todas_Alertas> response) {
                Todas_Alertas peticion = response.body();
                if (response.body() == null) {
                    Toast.makeText(VistaAlertas.this, "Ocurrió un Error Inténtalo más Tarde", Toast.LENGTH_LONG).show();
                    return;
                }

                //Arrays para id de alerta y usuario
                ArrayList<Integer> id = new ArrayList<>();
                ArrayList<Integer> usuarioId = new ArrayList<>();

                //Recorrer y almacenar id de alerta y usuarioId
                for (int i = 0; i < peticion.alertasList.size() - 1; i++) {
                    id.add(peticion.alertasList.get(i).id);
                    usuarioId.add(peticion.alertasList.get(i).usuarioId);
                }

                //Mostrar los id en el el listview
                ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(VistaAlertas.this, android.R.layout.simple_list_item_1, usuarioId);

                //Agregar adaptador a la lista
                ListaAlertas.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<Todas_Alertas> call, Throwable t) {
                Toast.makeText(VistaAlertas.this, "Error :(", Toast.LENGTH_SHORT).show();
            }
        });

    }
}