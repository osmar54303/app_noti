package com.example.app_noti.Api.Servicios;

import com.example.app_noti.Models.M_TodasAlertas;
import com.example.app_noti.ViewModels.Alertas_Usuario;
import com.example.app_noti.ViewModels.Crear_Alerta;
import com.example.app_noti.ViewModels.Peticion_Login;
import com.example.app_noti.ViewModels.Peticion_Notificaciones;
import com.example.app_noti.ViewModels.Registro_Usuario;
import com.example.app_noti.ViewModels.Todas_Alertas;
import com.example.app_noti.ViewModels.Todas_Visualizaciones;
import com.example.app_noti.ViewModels.Visualizaciones_Usuario;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ServicioPeticion {

    //Registrarse
    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<Registro_Usuario> registrarUsuario(@Field("username") String correo, @Field("password") String contrasenia);

    //Iniciar Sesión
    @FormUrlEncoded
    @POST("api/login")
    Call<Peticion_Login> getLogin(@Field("username") String correo, @Field("password") String contrasenia);

    //Crear Alertas
    @FormUrlEncoded
    @POST("api/crearAlerta")
    Call<Crear_Alerta> getCrearAlerta(@Field("usuarioId") String usuarioId);

    //Alertas Usuario
    @FormUrlEncoded
    @POST("api/alertasusuario")
    Call<Alertas_Usuario> getAlertasUsuario(@Field("usuarioId") String usuarioId);

    //Todas las Alertas
    @POST("api/alertas")
    Call<Todas_Alertas> getAlertas();

    //--Crear Alerta--//
    @FormUrlEncoded
    @POST("api/crearvisualizacionalerta")
    Call<Peticion_Notificaciones> getCrearVisualizacionAlerta(@Field("usuarioId") String usuarioId, @Field("alertaId") String alertaId);

    //Visualizaciones Usuario
    @FormUrlEncoded
    @POST("api/visualizacionesusuario")
    Call<Visualizaciones_Usuario> getVisualizacionesUsuario(@Field("usuarioId") String usuarioId);

    //Todas Visualizaciones
    @FormUrlEncoded
    @POST("api/visualizaciones")
    Call<Todas_Visualizaciones> getVisualizaciones();

}
