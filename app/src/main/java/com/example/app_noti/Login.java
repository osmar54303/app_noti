package com.example.app_noti;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.app_noti.Api.Api;
import com.example.app_noti.Api.Servicios.ServicioPeticion;
import com.example.app_noti.ViewModels.Peticion_Login;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {

    //Declarando variables de controles
    private EditText txtCorreo, txtContrasena;
    private Button btnIniciar, btnRegistro;
    private String APITOKEN = "";
    private String usuario_Id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Enlazando o casteando controles
        txtCorreo = findViewById(R.id.edtTxtCorreoL);
        txtContrasena = findViewById(R.id.edtTxtContraL);
        btnIniciar = findViewById(R.id.BtnIniciarL);
        btnRegistro = findViewById(R.id.BtnRegistrarse);

        // -- Verificar si tiene una sesión iniciada
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN", "");
        String id = preferencias.getString("usuarioId", "");
        if (token != "") {
            Toast.makeText(Login.this, "Bienvenido Nuevamente" + id  , Toast.LENGTH_LONG).show();
            startActivity(new Intent(Login.this, Menu.class));

        }

        //startActivity(new Intent(Login.this, Menu.class));
        //Iniciar Sesión
        btnIniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtCorreo.getText().toString().isEmpty() || txtCorreo.getText().toString() == "") {
                    txtCorreo.setSelectAllOnFocus(true);
                    txtCorreo.requestFocus();
                    mensajeError("Introduce un Correo");
                    return;
                }
                if (txtContrasena.getText().toString().isEmpty() || txtContrasena.getText().toString() == "") {
                    txtContrasena.setSelectAllOnFocus(true);
                    txtContrasena.requestFocus();
                    mensajeError("Introduce una Contraseña");
                    return;
                }

                //Servicio Petición Completo
                ServicioPeticion service = Api.getApi(Login.this).create(ServicioPeticion.class);
                Call<Peticion_Login> loginCall = service.getLogin(txtCorreo.getText().toString(), txtContrasena.getText().toString());
                loginCall.enqueue(new Callback<Peticion_Login>() {
                    @Override
                    public void onResponse(Call<Peticion_Login> call, Response<Peticion_Login> response) {
                        Peticion_Login peticion = response.body();
                        if (peticion.estado == "true") {
                            APITOKEN = peticion.token;
                            usuario_Id = peticion.id;
                            guardarPreferencias();
                            Toast.makeText(Login.this, "Bienvenido", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(Login.this, Menu.class));
                        } else {
                            Toast.makeText(Login.this, "Datos Incorrectos", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Peticion_Login> call, Throwable t) {
                        Toast.makeText(Login.this, "Error :(", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        //Boton para Registrarse
        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login.this, Registro.class));
            }
        });
    }

    private void mensajeError(String error) {
        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
    }

   /* private void cargarpreferencias() {
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN", "Haber que sale");
    }*/


    public void guardarPreferencias() {
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = APITOKEN;
        String id = usuario_Id;
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString("TOKEN", token);
        editor.putString("usuarioId", id);
        editor.apply();
    }
}
