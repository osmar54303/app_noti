package com.example.app_noti.ViewModels;

import com.example.app_noti.Models.M_AlertasUsuario;

import java.util.List;

public class Alertas_Usuario {

    public String estado;
    public List<M_AlertasUsuario> alertasUsuarioList;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<M_AlertasUsuario> getAlertasUsuario() {
        return alertasUsuarioList;
    }

    public void setAlertasUsuario(List<M_AlertasUsuario> alertasUsuario) {
        this.alertasUsuarioList = alertasUsuario;
    }
}
