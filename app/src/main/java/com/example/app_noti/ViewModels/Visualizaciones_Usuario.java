package com.example.app_noti.ViewModels;

import com.example.app_noti.Models.M_VisualizacionesUsuario;

import java.util.List;

public class Visualizaciones_Usuario {

    public String estado;
    public List<M_VisualizacionesUsuario> visualizacionesUsuarioList;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<M_VisualizacionesUsuario> getVisualizacionesUsuarioList() {
        return visualizacionesUsuarioList;
    }

    public void setVisualizacionesUsuarioList(List<M_VisualizacionesUsuario> visualizacionesUsuarioList) {
        this.visualizacionesUsuarioList = visualizacionesUsuarioList;
    }
}
