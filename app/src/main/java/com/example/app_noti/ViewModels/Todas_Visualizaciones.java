package com.example.app_noti.ViewModels;

import com.example.app_noti.Models.M_TodasVisualizaciones;

import java.util.List;

public class Todas_Visualizaciones {

    public List<M_TodasVisualizaciones> visualizacionesList;

    public List<M_TodasVisualizaciones> getVisualizacionesList() {
        return visualizacionesList;
    }

    public void setVisualizacionesList(List<M_TodasVisualizaciones> visualizacionesList) {
        this.visualizacionesList = visualizacionesList;
    }
}
