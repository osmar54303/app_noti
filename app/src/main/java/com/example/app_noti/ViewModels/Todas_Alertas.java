package com.example.app_noti.ViewModels;

import com.example.app_noti.Models.M_TodasAlertas;

import java.util.List;

public class Todas_Alertas {

    public List<M_TodasAlertas> alertasList;

    public List<M_TodasAlertas> getAlertasList() {
        return alertasList;
    }

    public void setAlertasList(List<M_TodasAlertas> alertasList) {
        this.alertasList = alertasList;
    }
}
