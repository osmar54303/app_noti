package com.example.app_noti;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.app_noti.Api.Api;
import com.example.app_noti.Api.Servicios.ServicioPeticion;
import com.example.app_noti.ViewModels.Crear_Alerta;
import com.example.app_noti.ViewModels.Todas_Alertas;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Menu extends AppCompatActivity {

    private Button BtnTodasAlertas, BtnAlertasUsuario, BtnCrearAlerta, BtnTodasVisualizaciones, BtnVisualizacionesUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        //Boton para Todas las Alertas
        BtnTodasAlertas = findViewById(R.id.Btn_TodasAlertas);
        //Boton para Alertas de Usuario
        BtnAlertasUsuario = findViewById(R.id.Btn_AlertasUsuario);
        //Boton para Agregar Alerta
        BtnCrearAlerta = findViewById(R.id.Btn_CrearAlerta);
        //Boton para Todas las Todas_Visualizaciones
        BtnTodasVisualizaciones = findViewById(R.id.Btn_TodasVisualizaciones);
        //Boton para Todas_Visualizaciones Usuario
        BtnVisualizacionesUsuario = findViewById(R.id.Btn_VisualizacionesUsuario);


        // -- Verificar si tiene una sesión iniciada
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN", "");
        if (token != "") {
            Toast.makeText(Menu.this, "Selecciona una Opcion", Toast.LENGTH_LONG).show();
        }

        //Mostrar Alertas del Usuario
        BtnAlertasUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        //Crear Alerta de Usuario
        BtnCrearAlerta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Verificar token e id
                SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
                String token = preferencias.getString("TOKEN", "");
                String id = preferencias.getString("usuarioId", "");
                if (token != "") {
                    Toast.makeText(Menu.this, "Tu usuarioId es: " + id, Toast.LENGTH_LONG).show();
                }

                ServicioPeticion service = Api.getApi(Menu.this).create(ServicioPeticion.class);
                Call<Crear_Alerta> crearAlertaCall = service.getCrearAlerta(id);
                crearAlertaCall.enqueue(new Callback<Crear_Alerta>() {
                    @Override
                    public void onResponse(Call<Crear_Alerta> call, Response<Crear_Alerta> response) {
                        Crear_Alerta peticion = response.body();
                        if (response.body() == null) {
                            Toast.makeText(Menu.this, "Ocurrió un Error Inténtalo más Tarde", Toast.LENGTH_LONG).show();
                            return;
                        }
                        if (peticion.estado == "true") {
                            Toast.makeText(Menu.this, "Se creó la Alerta", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(Menu.this, "Algo salió mal", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Crear_Alerta> call, Throwable t) {
                        Toast.makeText(Menu.this, "Error :(", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        //Mostrar Todas las Alertas
        BtnTodasAlertas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Menu.this, VistaAlertas.class));

            }
        });

        //Mostrar Alertas de Usuario
        BtnAlertasUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Menu.this, AlertasUsuario.class));
            }
        });

        //Mostrar Todas las Visualizaciones
        BtnTodasVisualizaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Menu.this, Visualizaciones.class));
            }
        });

        //Mostrar Visualizaciones Usuario
        BtnVisualizacionesUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Menu.this, VisualizacionesUsuario.class));
            }
        });

    }


}
