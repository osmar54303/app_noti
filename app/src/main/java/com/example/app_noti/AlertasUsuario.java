package com.example.app_noti;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.app_noti.Api.Api;
import com.example.app_noti.Api.Servicios.ServicioPeticion;
import com.example.app_noti.ViewModels.Alertas_Usuario;
import com.example.app_noti.ViewModels.Todas_Alertas;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AlertasUsuario extends AppCompatActivity {

    //Variables para la ListView
    ListView ListaAlertasUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alertas_usuario);

        //Enlazar variable con la vista del ListView
        ListaAlertasUsuario = findViewById(R.id.ListV_AlertasUsuario);

        // Verificar token
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN", "");
        String id = preferencias.getString("usuarioId", "");

        ServicioPeticion service = Api.getApi(AlertasUsuario.this).create(ServicioPeticion.class);
        Call<Alertas_Usuario> alertas_usuarioCall = service.getAlertasUsuario(id);
        alertas_usuarioCall.enqueue(new Callback<Alertas_Usuario>() {
            @Override
            public void onResponse(Call<Alertas_Usuario> call, Response<Alertas_Usuario> response) {
                Alertas_Usuario peticion = response.body();
                if (response.body() == null) {
                    Toast.makeText(AlertasUsuario.this, "Ocurrió un Error Inténtalo más Tarde", Toast.LENGTH_LONG).show();
                    return;
                }
                if (peticion.estado == "true") {

                    //Arrays para id de alerta y usuario
                    ArrayList<String> id = new ArrayList<>();
                    ArrayList<String> usuarioId = new ArrayList<>();
                    //Recorrer y almacenar id de alerta y usuarioId
                    for (int i = 0; i < peticion.alertasUsuarioList.size() - 1; i++) {

                        id.add(peticion.alertasUsuarioList.get(i).id);
                        usuarioId.add(peticion.alertasUsuarioList.get(i).usuarioId);
                    }
                    //Mostrar los id en el el listview
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(AlertasUsuario.this, android.R.layout.simple_list_item_1, id);
                    ListaAlertasUsuario.setAdapter(adapter);

                } else {
                    Toast.makeText(AlertasUsuario.this, "Algo salió mal", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<Alertas_Usuario> call, Throwable t) {
                Toast.makeText(AlertasUsuario.this, "Error :(", Toast.LENGTH_SHORT).show();
            }
        });

    }
}