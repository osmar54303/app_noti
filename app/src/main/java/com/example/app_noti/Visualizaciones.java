package com.example.app_noti;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.app_noti.Api.Api;
import com.example.app_noti.Api.Servicios.ServicioPeticion;
import com.example.app_noti.ViewModels.Todas_Alertas;
import com.example.app_noti.ViewModels.Todas_Visualizaciones;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Visualizaciones extends AppCompatActivity {

    //Variables para la ListView
    ListView ListaVisualizaciones;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizaciones);


        //Enlazar variable con la vista del ListView
        ListaVisualizaciones = findViewById(R.id.ListV_Visualizaciones);

        // Verificar token
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN", "");

        ServicioPeticion service = Api.getApi(Visualizaciones.this).create(ServicioPeticion.class);
        Call<Todas_Visualizaciones> todas_visualizacionesCall = service.getVisualizaciones();
        todas_visualizacionesCall.enqueue(new Callback<Todas_Visualizaciones>() {
            @Override
            public void onResponse(Call<Todas_Visualizaciones> call, Response<Todas_Visualizaciones> response) {
                Todas_Visualizaciones peticion = response.body();
                if (response.body() == null) {
                    Toast.makeText(Visualizaciones.this, "Ocurrió un Error Inténtalo más Tarde", Toast.LENGTH_LONG).show();
                    return;
                }

                //Arrays para id de alerta y usuario
                ArrayList<String> id = new ArrayList<>();
                ArrayList<String> usuarioId = new ArrayList<>();
                //Recorrer y almacenar id de alerta y usuarioId
                for (int i = 0; i < peticion.visualizacionesList.size() - 1; i++) {

                    id.add(peticion.visualizacionesList.get(i).id);
                    usuarioId.add(peticion.visualizacionesList.get(i).usuarioId);
                }
                //Mostrar los id en el el listview
                ArrayAdapter<String> adapter = new ArrayAdapter<>(Visualizaciones.this, android.R.layout.simple_list_item_1, usuarioId);
                ListaVisualizaciones.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<Todas_Visualizaciones> call, Throwable t) {
                Toast.makeText(Visualizaciones.this, "Error :(", Toast.LENGTH_SHORT).show();
            }
        });

    }
}