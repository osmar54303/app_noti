package com.example.app_noti;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.app_noti.Api.Api;
import com.example.app_noti.Api.Servicios.ServicioPeticion;
import com.example.app_noti.ViewModels.Alertas_Usuario;
import com.example.app_noti.ViewModels.Visualizaciones_Usuario;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VisualizacionesUsuario extends AppCompatActivity {

    //Variables para la ListView
    ListView ListaVisualizacionesUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizaciones_usuario);

        //Enlazar variable con la vista del ListView
        ListaVisualizacionesUsuario = findViewById(R.id.ListV_VisualizacionesUsuario);

        // Verificar token
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN", "");
        String id = preferencias.getString("usuarioId", "");

        ServicioPeticion service = Api.getApi(VisualizacionesUsuario.this).create(ServicioPeticion.class);
        Call<Visualizaciones_Usuario> visualizacionesUsuarioCall = service.getVisualizacionesUsuario(id);
        visualizacionesUsuarioCall.enqueue(new Callback<Visualizaciones_Usuario>() {
            @Override
            public void onResponse(Call<Visualizaciones_Usuario> call, Response<Visualizaciones_Usuario> response) {
                Visualizaciones_Usuario peticion = response.body();
                if (response.body() == null) {
                    Toast.makeText(VisualizacionesUsuario.this, "Ocurrió un Error Inténtalo más Tarde", Toast.LENGTH_LONG).show();
                    return;
                }
                if (peticion.estado == "true") {

                    //Arrays para id de alerta y usuario
                    ArrayList<String> id = new ArrayList<>();
                    ArrayList<String> usuarioId = new ArrayList<>();
                    //Recorrer y almacenar id de alerta y usuarioId
                    for (int i = 0; i < peticion.visualizacionesUsuarioList.size() - 1; i++) {

                        id.add(peticion.visualizacionesUsuarioList.get(i).id);
                        usuarioId.add(peticion.visualizacionesUsuarioList.get(i).usuarioId);
                    }
                    //Mostrar los id en el el listview
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(VisualizacionesUsuario.this, android.R.layout.simple_list_item_1, id);
                    ListaVisualizacionesUsuario.setAdapter(adapter);

                } else {
                    Toast.makeText(VisualizacionesUsuario.this, "Algo salió mal", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<Visualizaciones_Usuario> call, Throwable t) {
                Toast.makeText(VisualizacionesUsuario.this, "Error :(", Toast.LENGTH_SHORT).show();
            }
        });

    }
}